import java.util.Random;
import java.util.Scanner;


public class Main {
  public static void main(String[] args) {

    Random rand = new Random();
    Scanner scan = new Scanner(System.in);
    // Randomizers
    int login = 7;
    int games = 0;
    int players = 10;

    Queue queue = new Queue(10);

    clearScreen();
    System.out.println("Total Games: " + games);
    queue.printQueue();
    scan.nextLine();

    while (games < 10) {
      clearScreen();
      if (queue.size() >= 5) {
        clearScreen();
        for (int i = 0; i < 5; i++) {
          System.out.print(queue.remove());
          if (i != 4) System.out.print(", ");
        }
        System.out.println(" are starting a game!");
        scan.nextLine();
        games++;
      }
      int logins = rand.nextInt(login) + 1;
      for (int i = 0; i < logins; i++) {
        int generator = rand.nextInt(players);
        queue.add(new Player(generator));
      }
      clearScreen();
      System.out.println("Total Games: " + games);
      queue.printQueue();
      scan.nextLine();
    }
  }

  public static void clearScreen() {
    System.out.print("\033[H\033[2J");
    System.out.flush();
  }
}
