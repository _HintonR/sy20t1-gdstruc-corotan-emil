import java.util.Objects;
import java.util.Random;

public class Player {
  private int id;
  private String name;
  private int level;
  private double exp;

  public Player(int id) {
    Random rand = new Random();
    String name = "";

    switch(id) {
      case 0: name = "Jack"; break;
      case 1: name = "Sara"; break;
      case 2: name = "Matthew"; break;
      case 3: name = "Lucas"; break;
      case 4: name = "Graham"; break;
      case 5: name = "Adela"; break;
      case 6: name = "Kayla"; break;
      case 7: name = "Boris"; break;
      case 8: name = "Nick"; break;
      case 9: name = "Wendy"; break;
    }
    this.id = id;
    this.name = name;
    this.level = rand.nextInt(30) + 1;
  }

	public int getId() { return id; }
	public void setId(int id) { this.id = id; }

	public String getName() { return name; }
	public void setName(String name) { this.name = name; }

  public int getLevel() {	return level; }
  public void setLevel(int level)	{ this.level = level; }


	@Override
	public String toString() {
		return name + " | Lvl: " + level;
	}
}
