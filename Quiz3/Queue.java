import java.util.NoSuchElementException;

public class Queue {
  private Player[] queue;
  private int head;
  private int tail;

  public Queue(int capacity) { queue = new Player[capacity]; }

  public void add(Player player) {
    if (tail == queue.length) {
      Player[] newQueue = new Player[queue.length * 2];
      System.arraycopy(queue, 0, newQueue, 0, queue.length);
      queue = newQueue;
    }
    queue[tail] = player;
    tail++;
  }
  public Player remove() {
    if (size() == 0) { throw new NoSuchElementException(); }
    Player removed = queue[head];
    queue[head] = null;
    head++;
    if (size() == 0) {
      head = 0;
      tail = 0;
    }
    return removed;
  }

  public Player peek() {
    if (size() == 0) { throw new NoSuchElementException(); }
    return queue[head];
}

public void printQueue() {
  System.out.println("Queue: ");
  for (int i = head; i < tail; i++) {
    System.out.println(queue[i]);
  }
}

  public int size() { return tail - head; }

}
