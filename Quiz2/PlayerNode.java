public class PlayerNode {
  private Player p;
  private PlayerNode nP;
  private PlayerNode lP;

  public PlayerNode(Player p) {	this.p = p; }

  public Player getPlayer() {	return p; }
  public void setPlayer(Player p) { this.p = p; }

	public PlayerNode getNextPlayer() { return nP; }
	public void setNextPlayer(PlayerNode nP) { this.nP = nP; }

  public PlayerNode getLastPlayer() { return lP; }
	public void setLastPlayer(PlayerNode lP) { this.lP = lP; }

@Override
	public String toString() {
		return p.toString();
	}
}
