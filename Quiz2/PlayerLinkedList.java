public class PlayerLinkedList {
  private PlayerNode head;
  private PlayerNode temp;
  private PlayerNode last;
  private int size = 0;


  public void addToFront(Player p) {
    PlayerNode pNode = new PlayerNode(p);
    pNode.setNextPlayer(head);
    temp = pNode.getNextPlayer();
    pNode.setLastPlayer(temp);
    head = pNode;
    size++;
  }

  public void removeFront() {
    head = temp;
    temp = last;
    size--;
  }

  public void printList() {
    PlayerNode current = head;
    System.out.println("HEAD:");
    System.out.print(" -> ");
    while (current != null) {
      System.out.println(current);
      System.out.print(" -> ");
      current = current.getNextPlayer();
    }
    System.out.println("NULL");
    System.out.println("");
  }

  public boolean contains(Player p) {
    PlayerNode current = head;
    while (current != null) {
        if (current.getPlayer().equals(p)) return true;
        current = current.getNextPlayer();
    }
    return false;
  }

  public int indexOf(Player p) {
    PlayerNode current = head;
    int i = 0;
    while (current != null) {
      if (current.getPlayer().equals(p)) return i;
      current = current.getNextPlayer();
      i++;
      }
    return -1;
  }


	public PlayerNode getHead() { return head; }
	public void setHead(PlayerNode head) { this.head = head; }

  public PlayerNode getTemp() { return temp; }
  public void setTemp(PlayerNode temp) { this.temp = temp; }

  public PlayerNode getLast() { return head; }
  public void setLast(PlayerNode last) { this.last = last; }

  public int getSize() { return size; }
  public void setSize(int size) { this.size = size; }

}
