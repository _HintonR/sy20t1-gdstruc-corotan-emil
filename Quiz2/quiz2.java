import java.util.ArrayList;
import java.util.List;

public class quiz2 {
  public static void main(String[] args) {

  /*  List<Player> pList = new ArrayList<>();*/

    Player firion = new Player(1, "Firion", 10, 37.5);
    Player zidane =  new Player(2, "Zidane", 3, 97.66);
    Player kain = new Player(3, "Kain", 21, 24.12);

    PlayerLinkedList pL = new PlayerLinkedList();


    // pList.add(firion);
    // pList.add(zidane);
    // pList.add(kain);

  /*  for (Player p : pList) { System.out.println(p); }*/

    pL.addToFront(zidane);
    pL.addToFront(firion);
    pL.addToFront(kain);

    pL.removeFront();

    System.out.print("Size: ");
    System.out.println(pL.getSize());
    System.out.println("");

    pL.printList();

    System.out.println("Contains:");
    System.out.print("Firion: ");
    System.out.println(pL.contains(firion));
    System.out.print("Kain: ");
    System.out.println(pL.contains(kain));
    System.out.println("");

    System.out.println("Index:");
    System.out.print("Firion: ");
    System.out.println(pL.indexOf(firion));
    System.out.print("Zidane: ");
    System.out.println(pL.indexOf(zidane));
    System.out.print("Kain: ");
    System.out.println(pL.indexOf(kain));
  }
}
