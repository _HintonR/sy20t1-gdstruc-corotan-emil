import java.util.Random;
import java.util.Scanner;

public class Main {

  public static void main(String[] args) {

    Random rand = new Random();
    int job = 20;
    int action = 3;
    int cards = 5;

    int deckSize = 30;

    String name = "";

    Scanner scan = new Scanner(System.in);

    CardStack deck = new CardStack(deckSize);
    CardStack hand = new CardStack(1);
    CardStack pile = new CardStack(1);

    // Build Deck
    for (int i = 0; i <= deckSize; i++) {
      int x = rand.nextInt(job);
      switch(x) {
        case 0: name = "Novice"; break;
        case 1: name = "Reaper"; break;
        case 2: name = "Wizard"; break;
        case 3: name = "Vicar";  break;
        case 4: name = "Noble";  break;
        case 5: name = "Warrior"; break;
        case 6: name = "Duelist"; break;
        case 7: name = "Monk"; break;
        case 8: name = "Marksman"; break;
        case 9: name = "Corsair"; break;
        case 10: name = "Ranger"; break;
        case 11: name = "Paladin"; break;
        case 12: name = "Minstrel"; break;
        case 13: name = "Dragoon"; break;
        case 14: name = "Summoner"; break;
        case 15: name = "Scholar"; break;
        case 16: name = "Pirate"; break;
        case 17: name = "Viking"; break;
        case 18: name = "Myrmidon"; break;
        case 19: name = "Valkyrie"; break;
        }
      deck.push(new Card(name));
    }

    clearScreen();
    hand.printStack();
    if (deck.getSize() > 30) System.out.println("Deck: 30");
    else System.out.println("Deck: " + deck.getSize());
    if (hand.getSize() == 0 && pile.getSize() > 0) {
      int pileSize = pile.getSize() - 1;
      System.out.println("Pile: " + pileSize);
    }
    else if ( hand.getSize() == 1 && pile.getSize() > 0) {
      int pileSize = pile.getSize() + 1;
      System.out.println("Pile: " + pileSize);
    }
    else System.out.println("Pile: " + pile.getSize());

    while (deck.getSize() > 0) {
      int x = rand.nextInt(action);
      // Draw Card from Deck
      if (x == 0) {
        int num = rand.nextInt(cards) + 1;
        if (num > deck.getSize()) num = deck.getSize();
        for (int i = 1; i <= num; i++) {
          hand.push(deck.peek());
          deck.pop();
        }
      }
      // Discard Card
      if (x == 1) {
        if (hand.getSize() > 1) {
          int num = rand.nextInt(cards) + 1;
          if (num > hand.getSize()) num = hand.getSize();
          for (int i = 1; i <= num; i++) {
            pile.push(hand.peek());
            hand.pop();
          }
        }
      }
      // Draw Card from Discard Pile
      if (x == 2) {
        if (pile.getSize() > 1) {
          int num = rand.nextInt(cards) + 1;
          if (num > pile.getSize()) num = pile.getSize();
          for (int i = 1; i <= num; i++) {
            hand.push(pile.peek());
            pile.pop();
          }
        }
      }
      clearScreen();
      hand.printStack();
      if (deck.getSize() > 30) System.out.println("Deck: 30");
      else System.out.println("Deck: " + deck.getSize());
      if (hand.getSize() == 0 && pile.getSize() > 0) {
        int pileSize = pile.getSize() - 1;
        System.out.println("Pile: " + pileSize);
      }
      else if ( hand.getSize() == 1 && pile.getSize() > 0) {
        int pileSize = pile.getSize() + 1;
        System.out.println("Pile: " + pileSize);
      }
      else System.out.println("Pile: " + pile.getSize());
      scan.nextLine();
    }
  }

  public static void clearScreen() {
    System.out.print("\033[H\033[2J");
    System.out.flush();
  }
}
