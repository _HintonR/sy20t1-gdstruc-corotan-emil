import java.util.EmptyStackException;
import java.util.Objects;

public class CardStack {

  private Card[] stack;
  private int top;

  public CardStack(int capacity) { stack = new Card[capacity]; }

  public void push(Card card) {
    if (top == stack.length) {
      Card[] newStack = new Card[2 * stack.length];
      System.arraycopy(stack, 0, newStack, 0, stack.length);
      stack = newStack;
    }
    stack[top++] = card;
  }

  public Card pop() {
    if (isEmpty()) { throw new EmptyStackException(); }
    Card popCard = stack[--top];
    stack[top] = null;
    return popCard;
  }

  public Card peek() {
      if (isEmpty()) { throw new EmptyStackException(); }
      return stack[top - 1];
  }

  public void printStack() {
    System.out.print("Hand: ");
    for (int i = top - 1; i > 0; i--) {
      System.out.print(stack[i]);
      if (i != 1) { System.out.print(", "); }
    }
    System.out.println("");
  }

  public boolean isEmpty() { return top == 0; }

  public int getSize() { return top; }

}
