import java.util.Random;

public class quiz1 {

  public static void main(String[] args) {

    Random rand = new Random();
    int upperbound = 1000;

    int[] num = new int[10];
    int temp;

    // Populate Array
    for (int i = 0; i < num.length; i++) num[i] = rand.nextInt(upperbound);

    // Bubble Sort (Descending)
    int p1 = 0;
    int p2 = 0;
    for (int i = 0; i < num.length; i++) {
      for (int j = 0; j < num.length - i; j++) {
        p1 = j;
        if (j < num.length - 1) p2 = j + 1;
        if (num[p1] < num[p2]) {
          temp = num[p1];
          num[p1] = num[p2];
          num[p2] = temp;
        }
      }
    }

    // Print Array
    for (int i : num) System.out.println(i);

    // Selection Sort (Descending / Largest First)
    int l;
    for (int i = 0; i < num.length; i++) {
      l = i;
      for (int j = i + 1; j < num.length; j++)
        if (num[j] > num[l]) l = j; // Check for Largest
        temp = num[i];
        num[i] = num[l];
        num[l] = temp;
      }

      // Print Array
      for (int i : num) System.out.println(i);

      // Selection Sort (Descending / Smallest Last)
      int s;
      for (int i = num.length - 1; i >= 0; i--) {
        s = i;
        for (int j = i - 1; j >= 0; j--)
        if (num[j] < num[s]) s = j; // Check for Smallest
        temp = num[i];
        num[i] = num[s];
        num[s] = temp;
      }

      // Print Array
      for (int i : num) System.out.println(i);
  }
}
